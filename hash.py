import binascii

def calculate_crc32(filename):
    try:
        with open(filename, 'rb') as f:
            buf = f.read()
        return binascii.crc32(buf)
    except FileNotFoundError:
        return "Le fichier n'a pas été trouvé."

# Utilisation de la fonction
filename1 = "FileT10.hex" 
filename2 = "FileT11.hex" 
filename3 = "FileT13.hex" 
print(f"CRC32: {calculate_crc32(filename1):08X}")
print(f"CRC32: {calculate_crc32(filename2):08X}")
print(f"CRC32: {calculate_crc32(filename3):08X}")