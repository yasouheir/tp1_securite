import asyncio
from bleak import BleakClient

address = "F6:DB:D8:67:D1:EE"
#MODEL_NBR_UUID = "1b0d1200-a720-f7e9-46b6-31b601c4fca1"

async def main(address):
    client = BleakClient(address)
    try:
        await client.connect()
        #model_number = await client.read_gatt_char(MODEL_NBR_UUID)
       # print("Model Number: {0}".format("".join(map(chr, model_number))))
    except Exception as e:
        print(e)
    finally:
        await client.disconnect()

asyncio.run(main(address))
