import asyncio
from bleak import BleakClient, BleakScanner
import time
import struct
async def device(mac):
    # scan for devices
    devices = await BleakScanner.discover()
    for d in devices:
        if d.address == mac:
            return d
    return None

#byte_array = bytearray(b'T10\x00\x00\x00\x00\x00_\x08\x00\x00n\xe1\x0b\xfeH\x08er\xdc\x00\x00\x00')
#hex_string = ''.join('{:02x}'.format(byte) for byte in byte_array)
#print(hex_string)

def getNotified(sender,data):
    
    nom, size, hash, creationdate = struct.unpack('8sI4sQ', data)

# Affichage des données
    print(f"Nom: {nom.decode('utf-8')}")
    print(f"Size: {size}")
    print(f"Hash: {hash.hex()}")
    print(f"Creation Date: {creationdate}")

async def main():
    d = await device('F6:DB:D8:67:D1:EE')
    async with BleakClient(d.address) as client:
        if client.is_connected:
            ref = {}
            numValue = 0
            print('connected')
            services = await client.get_services()
            for service in services:
                for charac in service.characteristics:
                    temp = str(str(charac) + " UNKNOWN").split()
                    ref[temp[0] + '-' + temp[3]] = temp[1:][1][:-2]
            

                    
            for key in ref:
                if key.split('-')[-1] == 'LIST':
                    await client.start_notify(int(ref[key]), getNotified)

            
            # Write to the LIST characteristic first
            
            for key in ref:
                if key.split('-')[-1] == 'NUM':
                    numValue = await client.read_gatt_char(int(ref[key]))
            for key in ref:
                if key.split('-')[-1] == 'LIST':
                    
                    await client.write_gatt_char(int(ref[key]), numValue)
            for key in ref:
                if key.split('-')[-1] == 'READ':
            
                    await client.write_gatt_char(int(ref[key]), bytearray(b'T11\x00\x00\x00\x00\x00\x00\x00\x00\x00\x27\0x0f\x00\x00'))
            time.sleep(2)
            
           
            
asyncio.run(main())

