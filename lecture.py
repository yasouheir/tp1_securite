import asyncio
from bleak import BleakClient

ADDRESS = "F6:DB:D8:67:D1:EE" 
HANDLE_TO_READ = 66  # Handle de la caractéristique depuis laquelle lire

async def read_and_retrieve_files(address, read_handle, timeout=30):
    async with BleakClient(address) as client:
        value = await client.read_gatt_char(read_handle)
        
        print(value) 
        
        print("Données de la caractéristique lues avec succès et enregistrées dans un fichier texte.")

async def main():
    await read_and_retrieve_files(ADDRESS, HANDLE_TO_READ)

if __name__ == "__main__":
    asyncio.run(main())
