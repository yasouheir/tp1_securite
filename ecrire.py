import asyncio
from bleak import BleakClient

ADDRESS = "F6:DB:D8:67:D1:EE"  
HANDLE_TO_LIST = 58  # Handle de la caractéristique LIST
NUM_VALUE = bytearray([0x06, 0x00])  # Valeur en hexadécimal de la caractéristique NUM

async def write_to_list_characteristic(address, num_value, list_handle, timeout=30):
    async with BleakClient(address) as client:
        await client.write_gatt_char(list_handle, num_value, response=True)
        print("Valeur de la caractéristique NUM écrite avec succès sur la caractéristique LIST.")

async def main():
    await write_to_list_characteristic(ADDRESS, NUM_VALUE, HANDLE_TO_LIST)

if __name__ == "__main__":
    asyncio.run(main())
