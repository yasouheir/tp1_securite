import asyncio
from bleak import BleakClient

ADDRESS = "F6:DB:D8:67:D1:EE"  # Adresse de votre appareil Bluetooth

async def list_services(address, timeout=30):
    async with BleakClient(address) as client:
        services = await client.get_services()
        for service in services:
            print(f"Service UUID: {service.uuid}")
            for charactéristiques in service.characteristics:
                print(f"---------carachteristiques :{charactéristiques} ")
                


async def main():
    await list_services(ADDRESS)

if __name__ == "__main__":
    asyncio.run(main())

